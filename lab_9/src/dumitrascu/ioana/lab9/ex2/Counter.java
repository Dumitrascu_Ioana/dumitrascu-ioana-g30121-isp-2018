package dumitrascu.ioana.lab9.ex2;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.*;

public class Counter extends JPanel {
    private int count;
    private JButton push;
    private JLabel label;

    public Counter () {
        count = 0;
        push = new JButton ("Count!");
        push.addActionListener (new ButtonListener());
        label = new JLabel ("Counts: " + count);
        add (push);
        add (label);
        setPreferredSize (new Dimension(300, 40));
        setBackground (Color.pink);
    }

    private class ButtonListener implements ActionListener {
        public void actionPerformed (ActionEvent event)
        {
            count++;
            label.setText("Counts: " + count);
        }
    }
    public static void main (String[] args)
    {
        JFrame frame = new JFrame ("Counter");
        frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new Counter());
        frame.pack();
        frame.setVisible(true);
    }
}

