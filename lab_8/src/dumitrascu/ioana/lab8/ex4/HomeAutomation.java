package dumitrascu.ioana.lab8.ex4;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
//import java.util.Random;
import java.util.Scanner;

public class HomeAutomation {

    /*public static void main(String[] args){

        //test using an annonimous inner class
        Home h = new Home(){
            protected void setValueInEnvironment(Event event){
                System.out.println("New event in environment "+event);
            }
            protected void controllStep(){
                System.out.println("Control step executed");
            }
        };
        h.simulate();
    }
}*/

    public static void main(String[] args) throws FileNotFoundException {
        int threshold;
        Scanner s = new Scanner(System.in);
        System.out.println("Set temperature:");

        PrintStream out = new PrintStream(new File("system_logs.txt"));
        PrintStream console = System.out;


        System.setOut(out);
        threshold = s.nextInt();
        ControlUnit controlUnit = ControlUnit.getInstance(threshold);

        System.setOut(console);
        System.out.println("Home automation ended. Check system_logs.txt for further information.");
    }
}