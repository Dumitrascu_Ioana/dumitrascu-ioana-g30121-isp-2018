package dumitrascu.ioana.lab6.ex1;

public class Main {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Mihnea Ion",12000.5 );
        BankAccount b2 = new BankAccount("Mihnea Ion",12000.5 );
        BankAccount b3 = new BankAccount("Mirnea Ioan",12000.5 );
        BankAccount b4 = new BankAccount("Mihnea Andrei",1200.5 );
        BankAccount b5 = new BankAccount("Mihnea Ioana",2000.5 );
        BankAccount b6 = new BankAccount("Mihnea Ion",12000.5 );

        System.out.println("Bank Account: "+b1.equals(b6));
        System.out.println("Bank Account: "+b1.equals(b3));
        System.out.println("Bank Account: "+b2.equals(b5));
        System.out.println("Bank Account: "+b1.equals(b1));
        System.out.println("Bank Account: "+b3.equals(b3));
        System.out.println("Bank Account: "+b1.equals(b4));

        System.out.println("Bank Account: "+b1.hashCode());
    }
}
