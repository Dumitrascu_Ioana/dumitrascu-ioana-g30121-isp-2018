package dumitrascu.ioana.lab6.ex3;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Sandel Mircea", 2030);
        bank.addAccount("George Luca", 300);
        bank.addAccount("Dorian Popa", 700);
        bank.addAccount("Radu Mihaela", 450);
        bank.addAccount("Marian Marian",1000);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between the following limits: [2030,2040]");
        bank.printAccounts(2030,2040);
        System.out.println("Get Account by owner name");
        bank.getAccount("Radu Mihaela",450);
        System.out.println("Get All Account ordered by name");
        bank.getAllAccount();
    }
}
