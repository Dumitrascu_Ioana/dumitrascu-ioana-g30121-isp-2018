package dumitrascu.ioana.lab6.ex3;

public class BankAccount implements Comparable {
    public String owner;
    public double balance;

    public BankAccount(String owner, double balance) {
        this.balance = balance;
        this.owner = owner;
    }

    public void withdraw(double amount) {
        this.balance = this.balance - amount;
    }

    public void deposit(double amount) {
        this.balance = this.balance + amount;
    }

    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount bacc = (BankAccount) obj;
            return balance == bacc.balance && bacc.owner.equals(owner);
        }
        return false;
    }

    public int hashCode() {
        return (int) balance + owner.hashCode();
    }

    public int compareTo(Object o) {
        BankAccount a = (BankAccount) o;
        if (balance > a.balance) return 1;
        if (balance == a.balance) return 0;
        return -1;
    }

    public String toString() {
        return "(" + owner + ":" + balance + ")";
    }


}