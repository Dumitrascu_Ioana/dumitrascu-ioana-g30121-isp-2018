package dumitrascu.ioana.lab6.ex2;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Sandel Mircea", 2030);
        bank.addAccount("George Luca", 300);
        bank.addAccount("Dorian Popa", 200);
        bank.addAccount("Radu Mihaela", 450);
        bank.addAccount("Marian Marian",100);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between the following limits: [2030,2030]");
        bank.printAccounts(2030,2030);
        System.out.println("Get Account by owner name");
        bank.getAccount("Diana");
        System.out.println("Get All Account ordered by name");
        bank.getAllAccount();
    }
}
