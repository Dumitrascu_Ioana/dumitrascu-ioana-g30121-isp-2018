package dumitrascu.ioana.lab6.ex4;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {

        Dictionary dict = new Dictionary();
        char r;
        String l;
        String e;
        BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

        do {
            System.out.println("Menu");
            System.out.println("Adauga cuvant");
            System.out.println("Cauta cuvant");
            System.out.println("Printeaza cuvant");
            System.out.println("Arata definitia");
            System.out.println("Printeaza dictionar");
            System.out.println("Iesire");

            l = fluxIn.readLine();
            r = l.charAt(0);

            switch (r) {
                case 'a':
                case 'A':
                    System.out.println("Introduce the word:");
                    l = fluxIn.readLine();
                    if (l.length() > 1) {
                        System.out.println("Introduce definition:");
                        e = fluxIn.readLine();
                        dict.addWord(new Word(l), new Definition(e));
                    }
                    break;
                case 'c':
                case 'C':
                    System.out.println("Word found:");
                    l = fluxIn.readLine();
                    if (l.length() > 1) {
                        Word x = new Word(l);
                        e = String.valueOf(dict.getDefinition(x));

                        if (e == null)
                            System.out.println("The word was not found!");
                        else
                            System.out.println("Definition:" + e);
                    }
                    break;
                case 'l':
                case 'L':
                    System.out.println("Print: ");
                    dict.printDictionary();
                    break;
                case 'w':
                case 'W':
                    System.out.println("Get all words");
                    dict.getAllWords();
                    break;
                case 'd':
                case 'D':
                    System.out.println("Get all definitions");
                    dict.getAllDefinitions();
                    break;

            }
        } while (r != 'e' && r != 'E');
        System.out.println("Program finished. Thank you!");

    }

    }
