package dumitrascu.ioana.lab7.ex3;
import java.io.*;

public class Encrypt {
    public static void main(String[] args) throws IOException {
        File file = new File("C:\\Users\\Ioana\\Documents\\dumitrascu-ioana-g30121-isp-2018\\lab_7\\src\\dumitrascu\\ioana\\lab7\\ex3\\data.txt");
        File encfile = new File("C:\\Users\\Ioana\\Documents\\dumitrascu-ioana-g30121-isp-2018\\lab_7\\src\\dumitrascu\\ioana\\lab7\\ex3\\data.enc");
        File decfile = new File("C:\\Users\\Ioana\\Documents\\dumitrascu-ioana-g30121-isp-2018\\lab_7\\src\\dumitrascu\\ioana\\lab7\\ex3\\data.dec");
        BufferedReader bufferedreader = new BufferedReader(new FileReader(file));
        String line;
        StringBuilder builder=new StringBuilder();
        BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
        String command=reader.readLine();

        if(command.equals("encrypt")) {
            System.out.println(command);
            BufferedReader fileReader=new BufferedReader(new FileReader(file));
            BufferedWriter fileWriter= new BufferedWriter(new FileWriter(encfile));
            while ((line = fileReader.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) != ' ' && line.charAt(i) != '\n' && line.charAt(i) != '\0')
                        builder.append((char) (line.charAt(i) + 1));
                    else
                        builder.append(line.charAt(i));
                }
            }
            fileWriter.write(builder.toString());
            fileWriter.flush();
            fileWriter.close();
        }
        else if (command.equals("decrypt")){
            System.out.println(command);
            BufferedReader fileReader=new BufferedReader(new FileReader(encfile));
            BufferedWriter fileWriter= new BufferedWriter(new FileWriter(decfile));
            while ((line = fileReader.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) != ' ' && line.charAt(i) != '\n' && line.charAt(i) != '\0')
                        builder.append((char) (line.charAt(i) - 1));
                    else
                        builder.append(line.charAt(i));
                }
            }
            fileWriter.write(builder.toString());
            fileWriter.flush();
            fileWriter.close();
        }
    }
}
