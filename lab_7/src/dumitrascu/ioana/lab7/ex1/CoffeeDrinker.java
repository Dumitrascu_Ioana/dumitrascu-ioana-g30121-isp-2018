package dumitrascu.ioana.lab7.ex1;

public class CoffeeDrinker {
    void drinkCoffee(Coffee c) throws TemperatureException, ConcentrationException, CoffeeObjectsException {
        if (c.getTemp() > 60)
            throw new TemperatureException(c.getTemp(), "Coffee is to hot!");
        if (c.getConc() > 50)
            throw new ConcentrationException(c.getConc(), "Coffee concentration to high!");
        if (c.getCobj() > 10)
            throw new CoffeeObjectsException(c.getCobj(), "Too manny coffee objects!");
        System.out.println("Drink coffee:" + c);
    }
}

