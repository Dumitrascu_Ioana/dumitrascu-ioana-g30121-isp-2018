package dumitrascu.ioana.lab7.ex1;

public class CoffeeObjectsException extends Exception {
    int o;
    public CoffeeObjectsException(int o,String msg) {
        super(msg);
        this.o = o;
    }
    int getCobj(){
        return o;
    }
}
