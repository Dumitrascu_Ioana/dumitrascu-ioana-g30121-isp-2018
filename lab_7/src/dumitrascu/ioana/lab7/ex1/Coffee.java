package dumitrascu.ioana.lab7.ex1;

public class Coffee {
    private int temp;
    private int conc;
    private int cobj;

    Coffee(int t,int c,int o){temp = t;conc = c;cobj=o;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    int getCobj(){return cobj;}
    public String toString(){return "[coffee temperature="+temp+":concentration="+conc+":coffe objects="+cobj+"]";}
}

