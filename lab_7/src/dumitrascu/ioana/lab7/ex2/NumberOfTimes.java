package dumitrascu.ioana.lab7.ex2;
import java.io.*;

public class NumberOfTimes {
    public static void main(String[] args) throws IOException {
        System.out.println("Caracterul dat: ");
        InputStreamReader reader=new InputStreamReader(System.in);
        char caracter = (char) reader.read();
        char c='"';
        File file = new File("C:\\Users\\Ioana\\Documents\\dumitrascu-ioana-g30121-isp-2018\\lab_7\\src\\dumitrascu\\ioana\\lab7\\ex2\\data.txt");
        BufferedReader bufferedreader = new BufferedReader(new FileReader(file));
        String line;
        int count = 0;
        Boolean lowerCase=(caracter>='a' && caracter<='z');
        while((line=bufferedreader.readLine())!=null)
        {
            if(lowerCase)
                line=line.toLowerCase();
            else
                line=line.toUpperCase();
            for(int i=0;i<line.length();i++)
                if(line.charAt(i)==caracter) {
                    count++;
                }
        }
        System.out.println("Caracterul "+c+caracter+c+" apare de "+count+ " ori");
    }
}
