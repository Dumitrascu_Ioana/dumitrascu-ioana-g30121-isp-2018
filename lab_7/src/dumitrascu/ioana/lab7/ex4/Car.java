package dumitrascu.ioana.lab7.ex4;

import java.io.Serializable;

public class Car implements Serializable {
    private static final long serialVersionUID = 1L;
    private int price;
    private String model;

    Car(){};
    Car(int price, String model){
        this.price = price;
        this.model = model;
    }
    @Override
    public String toString() {
        return "Model:" + model + "\nPrice: " + price;
    }

}
