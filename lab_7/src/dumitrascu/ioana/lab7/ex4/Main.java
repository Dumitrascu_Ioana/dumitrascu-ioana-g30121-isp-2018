package dumitrascu.ioana.lab7.ex4;
import java.io.*;

public class Main {
    public static void main(String[] args) {
        Car c1 = new Car(12000,"BMV");
        Car c2 = new Car(250000,"Lamborghini");
        try {
            FileOutputStream f = new FileOutputStream(new File("myObjects.txt"));
            ObjectOutputStream o = new ObjectOutputStream(f);

            // Write objects to file
            o.writeObject(c1);
            o.writeObject(c2);

            o.close();
            f.close();

            FileInputStream fi = new FileInputStream(new File("myObjects.txt"));
            ObjectInputStream oi = new ObjectInputStream(fi);

            // Read objects
            Car cr1 = (Car) oi.readObject();
            Car cr2 = (Car) oi.readObject();

            System.out.println(cr1.toString());
            System.out.println(cr2.toString());

            oi.close();
            fi.close();
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        } catch (IOException e) {
            System.out.println("Error initializing stream");
        } catch (ClassNotFoundException e) {
            e.printStackTrace(); }

        /*String line;
        StringBuilder builder=new StringBuilder();
        BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
        String command=reader.readLine();
        if(command.equals("create car objects")) {
            System.out.println(command);
            BufferedReader fileReader=new BufferedReader(new FileReader(file));
            BufferedWriter fileWriter= new BufferedWriter(new FileWriter(encfile));
            while ((line = fileReader.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) != ' ' && line.charAt(i) != '\n' && line.charAt(i) != '\0')
                        builder.append((char) (line.charAt(i) + 1));
                    else
                        builder.append(line.charAt(i));
                }
            }
            fileWriter.write(builder.toString());
            fileWriter.flush();
            fileWriter.close(); */
        }
    }
