package dumitrascu.ioana.lab5.ex1;

public abstract class Shape {
    protected static String color;
    protected static boolean filled;

    public Shape() { }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor() {
        this.color = color;
    }

    public static boolean isFilled() {
        return filled;
    }

    public void setFilled() {
        this.filled = filled;
    }

    abstract double getArea(); //metode abstracte
    abstract double getPerimeter();
    abstract public String toString();
    }

