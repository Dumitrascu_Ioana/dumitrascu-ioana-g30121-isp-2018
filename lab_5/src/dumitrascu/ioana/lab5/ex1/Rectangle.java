package dumitrascu.ioana.lab5.ex1;

public class Rectangle extends Shape{
    protected double width;
    protected double length;

    public Rectangle(){
    }

    public Rectangle(double width, double length){
        this.width = width;
        this.length = length;
    }
    public Rectangle(String color, boolean filled, double width, double length){
        super(color,filled);
        this.width = width;
        this.length = length;
    }
    public double getWidth(){
        return width;
    }
    public void setWidth(){
        this.width = width;
    }
    public double getLength() {
        return length;
    }
    public void setLength(){
        this.length = length;
    }

    @Override
    public double getArea(){
        return this.length * this.width;
    }
    @Override
    public double getPerimeter(){
        return this.length * 2 + this.width * 2;
    }
    @Override
    public String toString(){
        return "A Rectangle with width = "+ this.width+ "and length =" +this.length;
    }
}
