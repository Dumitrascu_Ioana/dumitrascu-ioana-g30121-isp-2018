package dumitrascu.ioana.lab5.ex1;

public class TestClass {
    public static void main(String[] args)
    {
        Square s1 = new Square(2,"yellow",false);
        Rectangle r1 = new Rectangle(22,26);
        Circle c1 = new Circle(0.5);
        System.out.println("Square: "+s1.toString());
        System.out.println("Rectangle: "+r1.toString());
        System.out.println("Circle: "+c1.toString());
    }
}
