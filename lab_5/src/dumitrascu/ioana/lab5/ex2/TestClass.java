package dumitrascu.ioana.lab5.ex2;

public class TestClass {
    public static void main(String[] args){
        RealImage r = new RealImage("A");
        r.display();
        ProxyImage p = new ProxyImage("B");
        p.display();
        Image test = new RealImage("test");
        test.display();

        ProxyImage pr = new ProxyImage("D");
        pr.display();
        pr.RotatedImage();
        pr = new ProxyImage("D");
        pr.display();
    }
}
