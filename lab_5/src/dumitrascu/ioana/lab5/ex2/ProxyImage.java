package dumitrascu.ioana.lab5.ex2;

public class ProxyImage implements Image{

    private RealImage realImage;
    private String fileName;

    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        realImage.display();
    }
    public void RotatedImage(){
        System.out.println("Proxy image rotated"+fileName);
    }
}
