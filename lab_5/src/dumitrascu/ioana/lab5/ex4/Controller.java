package dumitrascu.ioana.lab5.ex4;
import dumitrascu.ioana.lab5.ex3.LightSensor;
import dumitrascu.ioana.lab5.ex3.TemperatureSensor;

import java.util.concurrent.TimeUnit;

class Controller {
    private static Controller single_instance = null;

    private Controller(){
    }
    public void control() throws InterruptedException {
        short count = 0;
        while (count < 20) {
            TimeUnit.SECONDS.sleep(1);
            System.out.println("TempSensor:" + TemperatureSensor.getTempSensor() + " si LightSensor:" + LightSensor.getKightSensor());
            count++;
        }
    }
    public static Controller getInstance() {
        if (single_instance == null) single_instance = new Controller();
        return single_instance;
    }

}