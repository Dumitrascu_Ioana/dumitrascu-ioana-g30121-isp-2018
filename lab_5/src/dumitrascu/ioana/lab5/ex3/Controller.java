package dumitrascu.ioana.lab5.ex3;

import java.util.concurrent.TimeUnit;

public class Controller {
    public void control() throws InterruptedException {
        short count = 0;
        while (count < 20) {
            TimeUnit.SECONDS.sleep(1);
            System.out.println("Temperature Sensor:" + TemperatureSensor.getTempSensor() + " and Light Sensor:" + LightSensor.getKightSensor());
            count++;
        }
    }
}
