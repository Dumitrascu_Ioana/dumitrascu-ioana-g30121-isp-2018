package dumitrascu.ioana.lab4.ex4;

import dumitrascu.ioana.lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args) {
        Author[] authors = new Author[2];
        authors[0] = new Author("Dan Balan","DanBalan@email", 'm');
        authors[1] = new Author("Balcesu Ion","Balcescu@email", 'm');
        Book b1 = new Book("Matematica ",authors,12.5);
        System.out.println(b1.toString());
        b1.printAuthors();
    }
}
