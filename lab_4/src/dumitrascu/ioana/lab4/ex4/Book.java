package dumitrascu.ioana.lab4.ex4;
import dumitrascu.ioana.lab4.ex2.Author;
public class Book {
    String name;
    Author[] authors;
    double price;
    int qtyInStock;

    public Book() {}
    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }
    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }
    public String getName() {
        return this.name;
    }
    public Author[] getAuthors() {
        return this.authors;
    }
    public double getPrice() {
        return this.price;
    }
    public void setPrice() {
        this.price = price;
    }
    public int getQtyInStock() { return this.qtyInStock; }
    public void setQtyInStock() { this.qtyInStock = qtyInStock; }

    public String toString() {
        int n = authors.length;
        return (name + " by " + n + " authors:") ;
    }
    void printAuthors(){
        Author[] authors = getAuthors();
        for(int i=0; i<authors.length; i++) System.out.println(authors[i].getName());
    }
}

