package dumitrascu.ioana.lab4.ex3;

import dumitrascu.ioana.lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args) {
        Author a1 = new Author( "G.G.Marquez", "ggm@gmail.com ", 'm' );
        Book b1 = new Book( "Pisica alba pisica neagra", a1 ,12 ,245);
        System.out.println(b1.toString());
    }
}
