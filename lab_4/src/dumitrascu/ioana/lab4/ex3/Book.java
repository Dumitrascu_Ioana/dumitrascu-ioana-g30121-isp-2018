package dumitrascu.ioana.lab4.ex3;
import dumitrascu.ioana.lab4.ex2.Author;

public class Book {
    String name;
    Author author;
    double price;
    int qtyInStock = 0;

    public Book (String name, Author author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }
    public Book (String name, Author author, double price, int qtyInStock) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName(){
        return this.name;
    }

    public Author getAuthor(){
        return this.author;
    }

    public double getPrice(){
        return this.price;
    }

    public void setPrice(){
        this.price = price;
    }

    public int getQtyInStock(){
        return this.qtyInStock;
    }

    public void setQtyInStock(){
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        return ("'" + name + "' by " + author) ;
    }
}
