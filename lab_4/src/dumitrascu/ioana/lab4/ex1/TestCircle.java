package dumitrascu.ioana.lab4.ex1;

public class TestCircle {
    public static void main (String[]args){
        Circle c1 = new Circle(1, "red");
        Circle c2 = new Circle(3, "blue");
        System.out.println("The area of circle c1 is: " +c1.getArea());
        System.out.println("The area of circle c2 is: " +c2.getArea());
    }
}
