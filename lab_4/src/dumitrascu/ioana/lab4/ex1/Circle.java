package dumitrascu.ioana.lab4.ex1;

import java.lang.Math.*;

public class Circle {
    public double radius = 1.0;
    String color = "red";

    public Circle(){}; //constructorii fara parametrii se fac pt clasele care mostenesc cercul ( cilindru ) ca sa se poata face super.
    public Circle(double radius, String color){
        this.radius = radius;
        this.color = color;
    }
    public double getRadius(){
        return this.radius;
    }
    public double getArea(){
        return Math.PI*(this.radius*this.radius);
    }
}
