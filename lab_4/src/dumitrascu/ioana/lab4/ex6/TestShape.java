package dumitrascu.ioana.lab4.ex6;

public class TestShape {
    public static void main(String[] args) {
        Shape s1 = new Shape("red", true);
        Square s2 = new Square(12,"green",true);
        System.out.println(s1.toString());
        System.out.println(s2.toString());
    }
}
