package dumitrascu.ioana.lab4.ex6;

public class Shape {
    private String color;
    private boolean filled;

    public Shape() {
        this.color = "red";
        this.filled = true;
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public double getArea(){return 0.;}; //abstracte
    public double getPerimeter() {return 0.;};

    @Override
    public String toString(){
        return ("A Shape with color of " + color + " filled? " + filled);
    }
}
