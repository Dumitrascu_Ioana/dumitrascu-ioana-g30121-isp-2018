package dumitrascu.ioana.lab4.ex5;

import dumitrascu.ioana.lab4.ex1.Circle;

public class Cylinder extends Circle {
    private double length;

    public Cylinder(){
        super();
        this.length = 1.0;
    }

    //public Cylinder(double length){
       // super();
       // this.length = length;
    //}

    public Cylinder(double radius){
        this.radius = radius;
    }

    public Cylinder (double length, double radius, String color) {
        super(radius,color);
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getRadius(){
        return radius;
    }

    public double getArea() {
        return 2*(Math.PI*(radius*radius))+ 2*Math.PI*radius*this.length;
    }
}
