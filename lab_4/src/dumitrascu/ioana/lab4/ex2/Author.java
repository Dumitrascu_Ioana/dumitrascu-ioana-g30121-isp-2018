package dumitrascu.ioana.lab4.ex2;

public class Author {
    String name;
    String email;
    char gender;
    public Author(String name, String email, char gender){
        this.name = name;
        this.email = email;
        this.gender = gender;
    }
    public String getName() {
        return name;
    }

    public String getEmail(){
        return this.email;
    }
    public char getGender(){
        return this.gender;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String toString() {
        return (name + "(" + gender + ")"  + "at email " + email)  ;
    }
}
