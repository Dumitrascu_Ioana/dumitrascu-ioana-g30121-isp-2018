package dumitrascu.ioana.lab2.ex3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class PrimeNumbers {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int A = in.nextInt();
        int B = in.nextInt();
        if (A > B) {
            int aux;
            aux = A;
            A = B;
            B = aux;
        }
        for (int n = A; n <= B; n++) {
            int divizori = 0;
            int d = 1;
            while (d <= n) {
                if (n % d == 0) {
                    divizori++;
                }
                d++;
            }
            if (divizori == 2) {
                System.out.println(n + " este prim!");
            } else {
                System.out.println(n + " nu este prim!");
            }
        }
    }
}



