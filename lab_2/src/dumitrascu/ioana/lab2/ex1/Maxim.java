package dumitrascu.ioana.lab2.ex1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Maxim {
        public static void main(String[] args){
            Scanner in = new Scanner(System.in);
            int x = in.nextInt();
            int y = in.nextInt();
            System.out.println("Numarul maxim este ");
            if(x>y)
                System.out.println("x=" + x);
            else
                System.out.println("y=" + y);
        }
}
