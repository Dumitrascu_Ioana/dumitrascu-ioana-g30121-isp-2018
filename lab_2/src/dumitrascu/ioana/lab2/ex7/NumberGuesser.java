package dumitrascu.ioana.lab2.ex7;

import java.util.Scanner;

//guess the number. system will generate a random number, user have 10 chances to guess it
public class NumberGuesser {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int randomNum =(int)(Math.random()*100);
        //System.out.println("Numarul de ghicit este:"+randomNum);
        for (int i = 0; i <= 9; i++) { //pt 3 sanse, i se ia pana la 2
            System.out.println("Introduceti numarul:");
            int num = in.nextInt();
            if (num > randomNum) System.out.println("Numarul este mai mare decat cel de ghicit.");
            else if (num == randomNum) {
                System.out.println("Bravo");
                i = 11; //pt 3 sanse i=4;
            }
            else System.out.println("Numarul este mai mic de cat cel de ghicit.");
            if(i==10)System.out.println("Fail"); // pt 3 sanse i=3
        }
    }
}
