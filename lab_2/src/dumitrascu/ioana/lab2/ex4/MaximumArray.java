package dumitrascu.ioana.lab2.ex4;

public class MaximumArray {
    public static void main(String[] args) {
        int[] array = {1,3,1,2,4,50,6,1,2,3,10};

        int max = array[0];
        for(int i=1; i<array.length; i++)
            if(array[i]>max) max=array[i];

        System.out.println("Maximul este:"+max);
    }
}
