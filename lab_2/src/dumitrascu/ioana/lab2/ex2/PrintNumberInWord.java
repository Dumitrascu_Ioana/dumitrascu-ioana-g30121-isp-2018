package dumitrascu.ioana.lab2.ex2;

public class PrintNumberInWord {
    public static void main(String[] args) {
        int number = 17;
        String numberString;
        switch (number) {
            case 1:  numberString = "1";
                System.out.println("ONE");
                break;
            case 2:  numberString = "2";
                System.out.println("TWO");
                break;
            case 3:  numberString = "3";
                System.out.println("THREE");
                break;
            case 4:  numberString = "4";
                System.out.println("FOUR");
                break;
            case 5:  numberString = "5";
                System.out.println("FIVE");
                break;
            case 6:  numberString = "6";
                System.out.println("SIX");
                break;
            case 7:  numberString = "7";
                System.out.println("SEVEN");
                break;
            case 8:  numberString = "8";
                System.out.println("EIGHT");
                break;
            case 9:  numberString = "9";
                System.out.println("NINE");
                break;
            default: numberString = "OTHER";
                System.out.println("OTHER");
                break;
        }
        System.out.println(numberString);
    }
}
