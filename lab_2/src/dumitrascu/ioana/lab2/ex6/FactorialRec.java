package dumitrascu.ioana.lab2.ex6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class FactorialRec {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Introduceti numarul:");
        int num = in.nextInt();
        class Calculation {
            int fact(int n) {
                int result;
                if (n == 1) return 1;
                result = fact(n - 1) * n;
                return result;
            }
        }
        Calculation obj = new Calculation();
        int a = obj.fact(num);
        System.out.println("N! = " + a);

    }
}
