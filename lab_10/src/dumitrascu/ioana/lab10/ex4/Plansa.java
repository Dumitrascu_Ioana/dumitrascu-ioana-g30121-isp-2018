package dumitrascu.ioana.lab10.ex4;

import java.util.ArrayList;

class Plansa{

    int dimX = 100;
    int dimY = 100;
    static int maxDim = Main.Robots.length;
    static ArrayList<Robot> FlaggedRobots = new ArrayList<Robot>();
    static int current = 0;

    public static void checkCollision(){
        boolean ok = true;
        for(int i = 0; i< maxDim; i++) {
            for (int j = 0; j < maxDim; j++) {
                if (i != j) {
                    if ((Main.Robots[i].PosX == Main.Robots[j].PosX) && (Main.Robots[i].PosY == Main.Robots[j].PosY)) {
                        for (Robot currentRobot : FlaggedRobots) {
                            if (Main.Robots[i].equals(currentRobot)) ok = false;
                            if (Main.Robots[j].equals(currentRobot)) ok = false;
                        }

                        if (ok == true) {
                            System.out.println(Main.Robots[i].nume + " collided with " + Main.Robots[j].nume + " and both exploded.");
                            Main.Robots[i].stop();
                            Main.Robots[j].stop();
                            FlaggedRobots.add(Main.Robots[i]);
                            FlaggedRobots.add(Main.Robots[j]);
                        }
                    }
                }
            }
        }
    }
}
