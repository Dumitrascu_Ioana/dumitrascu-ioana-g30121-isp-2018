package dumitrascu.ioana.lab3.ex2;

public class Circle {
    private double radius;
    private String color;
    Circle(){ //default
        this.radius=3.0;
        this.color="red";
    }
    Circle(double radius){
        this.radius = radius;
        this.color = "blue";
    }
    Circle(String color){
        this.radius = 2.5;
        this.color = color;
    }
    public double getRadius() {
        return this.radius;
    }
    public double getArea()
    {
        //double r=this.getRadius();
        return (3.14)*this.radius*this.radius;
    }
    public String getColor() {
        return this.color;
    }
}
