package dumitrascu.ioana.lab3.ex2;

public class TestCircle {
    public static void main(String[] args) {

        Circle c1 = new Circle(); //primul constructor
        Circle c2 = new Circle(1.0); // al doilea
        Circle c3 = new Circle("green"); //al treilea

        double r1 = c1.getRadius();
        double r2 = c2.getRadius();
        double r3 = c3.getRadius();

        double a1 = c1.getArea();
        double a2 = c2.getArea();
        double a3 = c3.getArea();

        System.out.println(String.format("Cercul 1 are raza %.0f si aria de %.2f",r1, a1));
        System.out.println(String.format("Cercul 2 are raza %.1f si aria de %.2f",r2,a2));
        System.out.println("Cercul 3 are raza " + r3 + " si aria " + a3);

        System.out.println("Cercul 3 are culoarea " + c3.getColor());
        System.out.println("Cercul 2 are culoarea " + c2.getColor());
    }
}
