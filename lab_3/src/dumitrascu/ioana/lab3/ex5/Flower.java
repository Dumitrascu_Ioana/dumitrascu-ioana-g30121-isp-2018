package dumitrascu.ioana.lab3.ex5;

public class Flower {
    int petala;
    static int nr = 0, counter, n;
    Flower(){
        System.out.println("Flower was created!");
    }
    static int count(int nr){
        counter = nr+1;
        return counter;
    }
    public static int getCounter() {
        return counter;
    }
    public static void main(String[] args){
        Flower[] garden = new Flower[200];
        for(int i=0; i<200; i++){
            counter = getCounter();
            Flower f = new Flower();
            n = Flower.count(counter);
            garden[i] = f;
        }
        System.out.println("There were created "+n+" objects");
    }
}
