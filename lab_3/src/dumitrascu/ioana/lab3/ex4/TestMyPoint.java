package dumitrascu.ioana.lab3.ex4;

public class TestMyPoint {
    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(1,2);
        MyPoint p3 = new MyPoint();
        System.out.println("Coordonatele punctului sunt:  " + p1.toString()+ " "+ p2.toString()+" " + p3.toString());
        p3.setXY(1 , 1);
        System.out.println("Distanta dintre doua puncte este: " + p2.distance(-2,1));
        System.out.println("Distanta dintre doua puncte este de coordonate " + p2.toString() + " si " + p1.toString() + " este : " + p2.distance(p1));

    }
}
