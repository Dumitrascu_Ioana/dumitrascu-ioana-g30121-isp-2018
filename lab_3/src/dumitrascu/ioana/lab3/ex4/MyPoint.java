package dumitrascu.ioana.lab3.ex4;
import java.util.Scanner;

public class MyPoint {
    Scanner a = new Scanner (System.in);
    int x = a.nextInt();
    int y = a.nextInt();

    MyPoint(){
        x = 0;
        y = 0;
        System.out.println("A fost creat un punct de coordonate ("+ x + " " +y + ")");
    }

    MyPoint(int x, int y){
        this.x = x;
        this.y = y;
        System.out.println("A fost creat un punct de coordonate ("+ x + "  " +y + ")");

    }
    public int getX()
    {
        System.out.println("Punctul are x de coordonata: "+x);
        return x;
    }

    public int getY() {
        System.out.println("Punctul are y de coordonata: "+y);
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setXY(int x,int y) {
        this.x = x;
        this.y = y;
        System.out.println("Punctul a fost setat de coordonate: ("+ x + " " + y+ ")");}


    public String toString()
    {
        return String.format("(%d, %d)", this.x, this.y);
    }
    public double distance(int x, int y) {
        int x1 = this.x - x;
        int y1 = this.y - y;
        return Math.sqrt(x1 * x1 + y1 * y1);
    }
    public double distance(MyPoint another) {
        int x1 = this.x - another.x;
        int y1 = this.y - another.y;
        return Math.sqrt(x1 * x1 + y1 * y1);
    }
}
