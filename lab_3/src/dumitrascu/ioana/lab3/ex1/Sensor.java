package dumitrascu.ioana.lab3.ex1;

public class Sensor{
    int value;
    Sensor(){
        value = -1;
    }
    public void change(int k){
        value = value + k;
    }
    public String toString(){
        return String.format("%d", value);
    }
}
