package dumitrascu.ioana.lab3.ex1;

public class TestSensor {
    public static void main(String[] args) {
        Sensor s1 = new Sensor();
        Sensor s2 = new Sensor();
        Sensor s3 = new Sensor();
        s1.change(5);
        s2.change(179);
        s3.change(-1);
        System.out.println("Sensor s1 value="+s1.toString());
        System.out.println("Sensor s2 value="+s2.toString());
        System.out.println("Sensor s3 value="+s3.toString());
    }
}
